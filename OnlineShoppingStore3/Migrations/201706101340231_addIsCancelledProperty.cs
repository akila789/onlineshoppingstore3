namespace OnlineShoppingStore3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIsCancelledProperty : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "IsDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "IsDeleted");
        }
    }
}
