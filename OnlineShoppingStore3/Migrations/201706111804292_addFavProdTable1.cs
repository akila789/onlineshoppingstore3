namespace OnlineShoppingStore3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFavProdTable1 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.FavoriteProducts");
            AddColumn("dbo.FavoriteProducts", "UserId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.FavoriteProducts", new[] { "ProductId", "UserId" });
            CreateIndex("dbo.FavoriteProducts", "ProductId");
            CreateIndex("dbo.FavoriteProducts", "UserId");
            AddForeignKey("dbo.FavoriteProducts", "ProductId", "dbo.Products", "ProductId", cascadeDelete: true);
            AddForeignKey("dbo.FavoriteProducts", "UserId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.FavoriteProducts", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FavoriteProducts", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.FavoriteProducts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FavoriteProducts", "ProductId", "dbo.Products");
            DropIndex("dbo.FavoriteProducts", new[] { "UserId" });
            DropIndex("dbo.FavoriteProducts", new[] { "ProductId" });
            DropPrimaryKey("dbo.FavoriteProducts");
            DropColumn("dbo.FavoriteProducts", "UserId");
            AddPrimaryKey("dbo.FavoriteProducts", "Id");
        }
    }
}
