namespace OnlineShoppingStore3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addFavProdTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FavoriteProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        IsFavourite = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FavoriteProducts");
        }
    }
}
