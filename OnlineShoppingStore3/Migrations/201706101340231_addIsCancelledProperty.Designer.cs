// <auto-generated />
namespace OnlineShoppingStore3.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class addIsCancelledProperty : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addIsCancelledProperty));
        
        string IMigrationMetadata.Id
        {
            get { return "201706101340231_addIsCancelledProperty"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
