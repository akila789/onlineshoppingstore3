﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OnlineShoppingStore3.Models;

namespace OnlineShoppingStore3.API
{
    public class ProductController : ApiController
    {
        ApplicationDbContext context;

        public ProductController()
        {
            context = new ApplicationDbContext();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var product = context.Products.Single(p => p.ProductId == id);

            product.IsDeleted = true;
            context.SaveChanges();
       
            return Ok();
        }
    }
}
