﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineShoppingStore3.Models;

namespace OnlineShoppingStore3.ViewModels
{
    public class ProductListViewModel
    {
        public IEnumerable<Products> Products { get; set; }
        public PagingInfo PagingInfo { get; set; }

        public int? CategoryId { get; set; }
    }
}