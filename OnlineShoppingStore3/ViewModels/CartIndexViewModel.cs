﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OnlineShoppingStore3.Models;
namespace OnlineShoppingStore3.ViewModels
{
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}