﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using OnlineShoppingStore3.Models;

namespace OnlineShoppingStore3.ViewModels
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name="Product")]
        public string ProductName { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [Required]
        public string Price { get; set; }

        public IEnumerable<Category> Categories { get; set; }
        
        [Required]
        [Display(Name = "Category")]
        public int CategoryId { get; set; }

        public string Heading { get; set; }

        public string Action
        {
            get
            {
                return (Id!=0) ? "Update":"Create";
            }
        }
            
    }
}