﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineShoppingStore3.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public void AddItem(Products product, int quantity)
        {
            CartLine line = lineCollection.Where(p => p.Products.ProductId == product.ProductId).FirstOrDefault();
            if (line==null)
            {
                lineCollection.Add(new CartLine { Products = product, Quantity = quantity });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveLine(Products product)
        {
            lineCollection.RemoveAll(p => p.Products.ProductId == product.ProductId);
        }

        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(p => p.Products.Price * p.Quantity);
        }

        public IEnumerable<CartLine> Line
        {
            get
            {
                return lineCollection;
            }
        }

        public void Clear()
        {
            lineCollection.Clear();
        }
    }
}