﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OnlineShoppingStore3.Models
{
    public class Products
    {
        [Key]
        public int ProductId { get; set; }
        
        [Required]
        public string ProductName { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

       public Category Category { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public bool IsDeleted { get; set; }
    }
}