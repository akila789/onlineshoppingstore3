﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineShoppingStore3.Models
{
    public class CartLine
    {
        public Products Products { get; set; }
        public int Quantity { get; set; }
    }
}