﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OnlineShoppingStore3.Models
{
    public class FavoriteProducts
    {
        public Products Product { get; set; }
        public ApplicationUser User { get; set; }

        [Key]
        [Column(Order=1) ]
        public int ProductId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string UserId { get; set; }

        public int IsFavourite { get; set; }
    }
}