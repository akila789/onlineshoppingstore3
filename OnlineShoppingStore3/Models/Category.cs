﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OnlineShoppingStore3.Models
{
    public class Category
    {
        [Key]
        public int CategoryId { get; set; }

        
        [Required]
        [StringLength(20)]
        public string CategoryName { get; set; }
    }
}
