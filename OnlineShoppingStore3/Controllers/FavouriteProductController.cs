﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using OnlineShoppingStore3.Models;
using Microsoft.AspNet.Identity;

namespace OnlineShoppingStore3.Controllers
{

    /*Use Chrome plugin Reslet to test the API 
     Headers Content-Type  application/json
     Request localhost:7334/api/FavouriteProduct  add value in body
     */

    public class FavProdDTO
    {
        public int ProductId { get; set; }
    }

    public class FavouriteProductController : ApiController
    {
        private ApplicationDbContext context;
        public FavouriteProductController()
        {
            context = new ApplicationDbContext();
        }

        [Authorize]
        [HttpPost]
        //FromBody - expects parameters from url
        public IHttpActionResult AddFavourite(FavProdDTO dto)
        {
            var userId = User.Identity.GetUserId();

            if (context.FavProducts.Any(a => a.UserId == userId && a.ProductId == dto.ProductId))
               return BadRequest("The attendance already exists.");


            var favProd = new FavoriteProducts
            {
                ProductId = dto.ProductId,
                  UserId = userId,
                  IsFavourite = 1
            };

            context.FavProducts.Add(favProd);
            context.SaveChanges();
            return Ok();
        }
    }
}
