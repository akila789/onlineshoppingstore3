﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingStore3.Models;
using OnlineShoppingStore3.ViewModels;
using System.Data.Entity;

namespace OnlineShoppingStore3.Controllers
{
    public class ProductController : Controller
    {

        ApplicationDbContext context;
        private int pageSize = 3;
        public ProductController()
        {
            context = new ApplicationDbContext();
        }

        [Authorize]
        public ActionResult Create()
        {
            var viewModel = new ProductViewModel
            {
                Categories = context.Categories.ToList(),
                Heading = "Add a Product"
            };
            return View("ProductForm",viewModel);
        }


        [Authorize]
        [HttpPost]
        public ActionResult Create(ProductViewModel viewModel)
        {

            if (!ModelState.IsValid)
            {
                viewModel.Categories = context.Categories.ToList();
                return View("ProductForm", viewModel);
            }

            var product = new Products
            {
                ProductName = viewModel.ProductName,
                Description = viewModel.Description,
                Price = Decimal.Parse(viewModel.Price),
                CategoryId = viewModel.CategoryId
            };

            context.Products.Add(product);
            context.SaveChanges();
            return RedirectToAction("Details", "Product");
        }

        [Authorize]
        public ActionResult Edit(int productId)
        {
            var product = context.Products.Single(p => p.ProductId == productId);

            var viewModel = new ProductViewModel
            {
                 Id = productId,
                 Categories = context.Categories.ToList(),
                 ProductName = product.ProductName,
                 Description = product.Description,
                 Price=product.Price.ToString(),
                 CategoryId=product.CategoryId,
                 Heading = "Edit a product"
               
            };
            return View("ProductForm", viewModel);
        }
        public ActionResult Details(int categoryId=0,int page = 1)
        {
            ProductListViewModel model = new ProductListViewModel
            {
                Products = context.Products
                    .Include(p => p.Category)
                    .Where(p => (categoryId == 0 || p.CategoryId==categoryId) & p.IsDeleted==false)
                    
                    .OrderBy(p => p.ProductId)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = context.Products.Where(p=>p.IsDeleted == false).Count()
                },
                 CategoryId = categoryId
            };

            return View(model);
        }


        [Authorize]
        [HttpPost]
        public ActionResult Update(ProductViewModel viewModel)
        {

            if (!ModelState.IsValid)
            {
                viewModel.Categories = context.Categories.ToList();
                return View("ProductForm", viewModel);
            }

            var product = context.Products.Single(p => p.ProductId == viewModel.Id);

            product.ProductName = viewModel.ProductName;
            product.Description = viewModel.Description;
            product.Price =Decimal.Parse(viewModel.Price);
            product.CategoryId = viewModel.CategoryId;
            
            context.SaveChanges();
            return RedirectToAction("Details", "Product");
        }

        public ActionResult Delete(int productId)
        {
            var product = context.Products.Single(p => p.ProductId == productId);
            product.IsDeleted = true;
            context.SaveChanges();
            return RedirectToAction("Details", "Product");
        }
    }
}