﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingStore3.Models;
using OnlineShoppingStore3.ViewModels;

namespace OnlineShoppingStore3.Controllers
{
    public class CartController : Controller
    {
        private ApplicationDbContext context;
        public CartController()
        {
            context = new ApplicationDbContext();
        }

        public PartialViewResult Summary(Cart cart)
        {
            return PartialView(GetCart());
        }
        public RedirectToRouteResult AddToCart(int productId,string returnUrl)
        {
            //get product
            Products product = context.Products.FirstOrDefault(p => p.ProductId == productId);
            if (product!=null)
            {
                GetCart().AddItem(product, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel { Cart = GetCart(), ReturnUrl = returnUrl });
        }
        public RedirectToRouteResult RemoveFromCart(int productId,string returnUrl)
        {
            Products product = context.Products.FirstOrDefault(p => p.ProductId == productId);
            if (product!=null)
            {
                GetCart().RemoveLine(product);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
        private Cart GetCart()
        {
            Cart cart = (Cart)Session["Cart"];
            if (cart==null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }

            return cart;
        }
    }
}