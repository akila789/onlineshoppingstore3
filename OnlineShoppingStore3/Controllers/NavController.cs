﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OnlineShoppingStore3.Models;

namespace OnlineShoppingStore3.Controllers
{
    
    public class NavController : Controller
    {
        ApplicationDbContext context;

        public NavController()
        {
            context = new ApplicationDbContext();
        }
        // GET: Nav
        public PartialViewResult Menu()
        {
            var categoriesList = context.Categories.ToList();
            return PartialView(categoriesList);
        }
    }
}